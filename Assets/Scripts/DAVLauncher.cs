﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

public class DAVLauncher : MonoBehaviour
{
	GameObject root;
	UnityEngine.UI.Text uilInfo, snInfo, txtPercentage, txtSN;
	UnityEngine.UI.Slider uisbProgressBar;
	int RegisterState = 0, CheckVersionState = 0, UpdateVersionState = 0;
    string WEB_ADDRESS = "http://182.253.226.170/";
    //string WEB_ADDRESS = "http://10.0.0.241/";
    string WEB_SERVICE_URL = string.Empty;

	WWW download;
    string serial = string.Empty;
    bool isDownload = false;
    bool isInit = false;
    bool isInitPassed = false;


    jAPKVersion apkVersion = null;
    jDAVAddress davAddress = null;

    int ConnectErrorCount = 0;
    string APKPath = string.Empty;

    NativeCall nc = null;

    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        WEB_SERVICE_URL = WEB_ADDRESS + "dav/";
        APKPath = Application.persistentDataPath + "/DAV.apk";
        root = GameObject.Find("Canvas");
        uilInfo = root.transform.Find("Desc").gameObject.GetComponent<UnityEngine.UI.Text>();
        txtSN = root.transform.Find("SerialNumber").gameObject.GetComponent<UnityEngine.UI.Text>();
        //snInfo = root.transform.Find("Panel/SerialInfo").gameObject.GetComponent<UILabel>();
        txtPercentage = root.transform.Find("ProgressBar/Percentage").gameObject.GetComponent<UnityEngine.UI.Text>();
        uisbProgressBar = root.transform.Find("ProgressBar").gameObject.GetComponent<UnityEngine.UI.Slider>();
        root.transform.Find("ProgressBar").gameObject.SetActive(false);
        uilInfo.text = "DAV Initialization";
        nc = new NativeCall();
    }
	// Use this for initialization
	void Start()
    {
        StartCoroutine(Init());
	}


    IEnumerator Init()
    {
        var download = new WWW(WEB_ADDRESS + "dav.domain/dav.json");
        yield return download;
        bool isSuccess = false;
        if (string.IsNullOrEmpty(download.error))
        {
            davAddress = new jDAVAddress();
            davAddress = JsonConvert.DeserializeObject<jDAVAddress>(download.text);
            isSuccess = true;
            WEB_SERVICE_URL = "http://" + davAddress.domain + ":" + davAddress.port + "/" + davAddress.directory + "/";
            Debug.Log(WEB_SERVICE_URL);
            uilInfo.text = "Initialization Success...";
        }
        else
        {
            Debug.Log(download.error);
            uilInfo.text = "Initialization Failed...";
            yield return new WaitForSeconds(1);
            uilInfo.text = "Re-Init...";
        }
        yield return new WaitForSeconds(2);

        if (isSuccess)
        {
            isInit = true;
        }
        else
        {
            ConnectErrorCount++;
            StartCoroutine(Init());
        }
            
    }
	
	// Update is called once per frame
	void Update()
	{
        if (isInit)
        {
            //snInfo.text = SystemInfo.deviceUniqueIdentifier.ToString();
            RegisterState = 0;
            serial = SystemInfo.deviceUniqueIdentifier.ToString();
            Debug.Log(serial);
            if (RegisterState == 0)
                StartCoroutine(RegisterDevice());

            isInit = false;
            isInitPassed = true;
        }

        if(isInitPassed)
        {
            if (isDownload)
            {
                root.transform.Find("ProgressBar").gameObject.SetActive(true);
                Debug.Log("progress : " + download.progress);
                //uilByteLeft.text = "(" + download.bytesDownloaded + "/" + download.size + ")";
                uisbProgressBar.value = download.progress * 100;
                txtPercentage.text = (download.progress * 100).ToString() + "%";
            }
            else
            {
                root.transform.Find("ProgressBar").gameObject.SetActive(false);
            }
            if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
        }

        if (ConnectErrorCount == 5)
        {
            StartCoroutine(Error());
        }
	}

    public IEnumerator Error()
    {
        uilInfo.text = "Force Run DAV";
        yield return new WaitForSeconds(2);
        StopAllCoroutines();
        StartCoroutine(NativeCall.Launch());
    }

    public IEnumerator RegisterDevice()
	{
        Debug.Log("Check current registry device number in server...");
        uilInfo.text = "Check current registry device number in server...";
        yield return new WaitForSeconds(2);
        bool isSuccess = false;
        download = new WWW(WEB_SERVICE_URL + @"mobile/register_device/" + this.serial);
        // check is device registered or not
        
		yield return download;
			
		if(string.IsNullOrEmpty(download.error))
		{
            Debug.Log(WEB_SERVICE_URL + @"mobile/register_device/" + this.serial);
            Debug.Log(download.text);
            bool parsePass = false;
            try
            {
                int.Parse(download.text);
                parsePass = true;
            }
			catch(Exception E)
            {
                Debug.Log(download.text + " Err : " + E.ToString());
            }
            if (parsePass)
            {
                if (int.Parse(download.text) == 1)
                {
                    Debug.Log("The device registry number not registered yet");
                    uilInfo.text = "The device registry number not registered yet";
                    yield return new WaitForSeconds(2);
                    Debug.Log("Automatic Device Registering...");
                    uilInfo.text = "Automatic Device Registering...";
                    yield return new WaitForSeconds(2);
                    Debug.Log("This device has just been registered.");
                    uilInfo.text = "This device has just been registered. Re-check...";
                    yield return new WaitForSeconds(2);
                    StartCoroutine(RegisterDevice());
                }
                else if (int.Parse(download.text) == 0)
                {
                    Debug.Log("This device has been registered.");
                    uilInfo.text = "This device already registered";
                    yield return new WaitForSeconds(2);
                }
                isSuccess = true;
            }
            else
            {
                uilInfo.text = "Device Registry call success, but data parsing fail to read. Please check the server device registry function";
                yield return new WaitForSeconds(2);
                uilInfo.text = "Recheck device registry";
                yield return new WaitForSeconds(2);
                StartCoroutine(CheckAPPsVersion());
                StartCoroutine(RegisterDevice());
            }

            
		}
		else
		{
            uilInfo.text = "Registering Device Connection Error";
            ConnectErrorCount++;
            yield return new WaitForSeconds(2);
		}

        if (!isSuccess)
        {
            Debug.Log("Connection Error. Re-connect...");
            uilInfo.text = "Connection Error. Re-connect...";
            yield return new WaitForSeconds(1);
            StartCoroutine(RegisterDevice());
        }
        else
        {
            StartCoroutine(CheckAPPsVersion());
        }
	}

    public IEnumerator CheckAPPsVersion()
    {
        bool isSuccess = false;
        bool isUpdated = false;
        Debug.Log("Check current DAV version in server...");
        uilInfo.text = "Check current DAV version in server...";
        yield return new WaitForSeconds(2);
        download = new WWW(WEB_SERVICE_URL + @"mobile/version_check/" + this.serial);
        yield return download;
        if (string.IsNullOrEmpty(download.error))
        {
            Debug.Log(download.text);
            apkVersion = JsonConvert.DeserializeObject<jAPKVersion>(download.text);
            Debug.Log("apkmd5 " + apkVersion.md5);

            if(apkVersion.id!=0)
            {
                //int.Parse(download.text);
                
                Debug.Log("This DAV Apps already updated version");
                uilInfo.text = "This DAV Apps already updated version";
                yield return new WaitForSeconds(2);
                isUpdated = true;
            }
            else
            {
                Debug.Log("The DAV Apps not updated yet");
                uilInfo.text = "The DAV Apps not updated yet";
                yield return new WaitForSeconds(2);
                isUpdated = false;
                // downloading apk for new update
            }

            if (!isUpdated)
                StartCoroutine(CheckLocalInstaller());
            else
                StartCoroutine(CheckInstalledApps());

            CheckVersionState = 1;
            isSuccess = true;
        }
        else
        {
            ConnectErrorCount++;
            uilInfo.text = "Check APP Version Connection Error";
            CheckVersionState = 2;
        }
        if (!isSuccess)
        {
            Debug.Log("Connection Error. Re-connect...");
            uilInfo.text = "Connection Error. Re-connect...";
            yield return new WaitForSeconds(1);
            StartCoroutine(CheckAPPsVersion());
        }
    }

    public IEnumerator DownloadAPPs()
    {
        bool isSuccess = false;
        isDownload = true;

        Debug.Log("Downloading The Latest DAV...");
        uilInfo.text = "Downloading The Latest DAV...";
        //yield return new WaitForSeconds(1);
        string path = Application.persistentDataPath + "/DAV.apk";

        Debug.Log(WEB_SERVICE_URL + @"apk/DAV_" + apkVersion.version + ".apk");
        download = new WWW(WEB_SERVICE_URL + @"apk/DAV_" + apkVersion.version + ".apk");

        yield return download;
        if (string.IsNullOrEmpty(download.error))
        {
            
            //Debug.Log (download.progress);
            if (download.progress == 1)
            {
                Debug.Log("The Latest DAV has just been downloaded");
                uilInfo.text = "The Latest DAV has just been downloaded";
                yield return new WaitForSeconds(2);
                isDownload = false;
            }
            //Debug.Log(Application.persistentDataPath);
            //uilInfo.text = Application.persistentDataPath;
            bool isSuccessed = false;
            try
            {
                System.IO.File.WriteAllBytes(path, download.bytes);
                isSuccessed = true;
            }
            catch 
            {
                //uilInfo.text = E.ToString();
                uilInfo.text = "Failed to write downloaded data";
                
            }

            yield return new WaitForSeconds(1);
            if (isSuccessed)
                StartCoroutine(InstallingLatestAPPs());
            else
                StartCoroutine(DownloadAPPs());

            isSuccess = true;
        }
        else
        {
            ConnectErrorCount++;
            uilInfo.text = "Download DAV Connection Error";
        }

        if (!isSuccess)
        {
            Debug.Log("Connection Error. Re-connect...");
            uilInfo.text = "Connection Error. Re-connect...";
            yield return new WaitForSeconds(1);
            StartCoroutine(DownloadAPPs());
        }
    }

    int InstallCount = 0;
    public IEnumerator InstallingLatestAPPs()
    {
        InstallCount++;
        Debug.Log("Installing DAV Latest version...");
        uilInfo.text = "Installing DAV Latest version......";
        yield return new WaitForSeconds(2);
        yield return StartCoroutine(NativeCall.Install(APKPath));
        uilInfo.text = "Finish Installing, wait the result......";
        yield return new WaitForSeconds(2);
        if (NativeCall.InstallResult)
        {
            uilInfo.text = "DAV has just been Installed, Re-Check DAV APPs Version...";
            yield return new WaitForSeconds(2);
            StartCoroutine(UpdateAPPsVersion());
        }
        else
        {
            uilInfo.text = "DAV has fail to Install, please wait to re-install...";
            yield return new WaitForSeconds(2);
            if (InstallCount > 3)
            {
                InstallCount++;
                StartCoroutine(InstallingLatestAPPs());
            }
            else
            {
                uilInfo.text = "Installing DAV fail recursively, Please check your device...";
                yield return new WaitForSeconds(2);
                Application.Quit();
            }
        }
    }

    /*public IEnumerator InstallingLatestAPPsWithoutUpdate()
    {
        InstallCount++;
        Debug.Log("Installing DAV Latest version...");
        uilInfo.text = "Installing DAV Latest version......";
        yield return new WaitForSeconds(2);
        yield return StartCoroutine(NativeCall.Install(APKPath));
        uilInfo.text = "Finish Installing, wait the result......";
        yield return new WaitForSeconds(2);

        if (NativeCall.InstallResult)
        {
            uilInfo.text = "DAV has just been Installed";
            yield return new WaitForSeconds(2);
            StartCoroutine(CheckInstalledApps());
        }
        else
        {
            uilInfo.text = "DAV has fail to Install, please wait to re-install..." + NativeCall.InstallResult;
            yield return new WaitForSeconds(2);
            if (InstallCount < 3)
            {
                InstallCount++;
                StartCoroutine(InstallingLatestAPPs());
            }
            else
            {
                uilInfo.text = "Installing DAV fail recursively, Please check your device...";
                yield return new WaitForSeconds(2);
                Application.Quit();
            }
        }
    }*/

    public IEnumerator UpdateAPPsVersion()
    {
        bool isSuccess = false;
        Debug.Log("Updating DAV Version in server...");
        uilInfo.text = "Updating DAV Version in server......";
        yield return new WaitForSeconds(2);

        download = new WWW(WEB_SERVICE_URL + @"mobile/update/" + serial);
        yield return download;
        if (string.IsNullOrEmpty(download.error))
        {
            //Debug.Log(download.text);
            Debug.Log("DAV Version in server has just been updated.");
            uilInfo.text = "DAV Version in server has just been updated.";
            isSuccess = true;
            yield return new WaitForSeconds(2);
            StartCoroutine(CheckInstalledApps());
        }
        else
        {
            ConnectErrorCount++;
            uilInfo.text = "Update DAV version Connection Error";
        }
        /*Debug.Log("Installing Latest APPs...");
        uilInfo.text = "Installing Latest APPs...";
        yield return new WaitForSeconds(2);*/
        if (!isSuccess)
        {
            Debug.Log("Connection Error. Re-connect...");
            uilInfo.text = "Connection Error. Re-connect...";
            yield return new WaitForSeconds(1);
            StartCoroutine(DownloadAPPs());
        }
    }

    int CheckInstalledCount = 0;

    /*IEnumerator CheckInstalledApps()
    {
        Debug.Log("Check Installed DAV APPs...");
        uilInfo.text = "Check Installed DAV APPs...";
        yield return new WaitForSeconds(2);
        nc = new NativeCall();
        nc.Launch();
        uilInfo.text = "launched";
    }*/
    bool IsExist = true;
    bool IsExistNext = false;
    IEnumerator CheckInstalledApps()
    {
        
        CheckInstalledCount++;
        Debug.Log ("Check Installed DAV APPs...");
		uilInfo.text = "Check Installed DAV APPs...";
        yield return new WaitForSeconds(2);

        uilInfo.text = "Launch DAV...";
        yield return new WaitForSeconds(2);
        StartCoroutine(NativeCall.Launch());
        yield return new WaitForSeconds(2);
        uilInfo.text = string.Empty;
        if (NativeCall.LaunchResult)
        {
            IsExist = true;
        }
        else
        {
            //IsExistNext = false;
            IsExist = false;
        }


        if (!IsExist)
        {
            uilInfo.text = "The DAV isn't Exist on current device";
            yield return new WaitForSeconds(2);
            StartCoroutine(CheckLocalInstaller());
        }
        else
        {
            //yield return StartCoroutine(NativeCall.Launch());
            /*uilInfo.text = "Re-launch DAV";
            yield return new WaitForSeconds(2);
            uilInfo.text = "Re-launch DAV in 3 seconds...";
            yield return new WaitForSeconds(1);
            uilInfo.text = "Re-launch DAV in 2 seconds...";
            yield return new WaitForSeconds(1);
            uilInfo.text = "Re-launch DAV in 1 seconds...";
            yield return new WaitForSeconds(1);
            uilInfo.text = "Re-launch DAV in 0 seconds...";
            yield return new WaitForSeconds(1);
            StartCoroutine(CheckInstalledApps());*/
            uilInfo.text = "Reset State";
            yield return new WaitForSeconds(2);
            this.Reset();
        }
    }

    IEnumerator CheckLocalInstaller()
    {
        uilInfo.text = "Check DAV Local Installer";
        yield return new WaitForSeconds(2);
        if (!System.IO.File.Exists(APKPath))
        {
            uilInfo.text = "Local Installer doesn't Exist...";
            yield return new WaitForSeconds(2);
            StartCoroutine(DownloadAPPs());
        }
        else
        {
            uilInfo.text = "Check Installer Validation";
            yield return new WaitForSeconds(2);
            if (!this.apkVersion.md5.Equals(Utilities.GetMD5Checksum(APKPath)))
            {
                uilInfo.text = "MD5 Checksum Error...";
                yield return new WaitForSeconds(2);
                StartCoroutine(DownloadAPPs());
            }
            else
            {
                StartCoroutine(InstallingLatestAPPs());
            }
        }

    }


    public void ShowDeviceRegistryNumber()
    {
        txtSN.text = serial;
        txtSN.enabled = !txtSN.enabled;
        //Application.LoadLevel("DeviceRegistering");
        //ErrorMsg.New("Triple Finger", "Double Tap");
        //SpawnParticles( doubleTapObject );
        //UI.StatusText = "Double-Tapped with finger " + gesture.Fingers[0];
    }

    public void QuitApps()
    {
        StopAllCoroutines();
        Debug.Log("Exit");
        Application.Quit();
    }

    public void Reset()
    {
        StopAllCoroutines();
        StartCoroutine(iReset());
    }

    IEnumerator iReset()
    {
        txtSN.text = "Reset";
        txtSN.enabled = true;
        yield return new WaitForSeconds(2);
        txtSN.enabled = false;
        isInit = false;
        isInitPassed = false;
        ConnectErrorCount = 0;
        RegisterState = 0;
        UpdateVersionState = 0;
        CheckVersionState = 0;
        StartCoroutine(Init());
    }

    public void ResetDevice()
    {
        StopAllCoroutines();
        StartCoroutine(iResetDevice());
    }

    IEnumerator iResetDevice()
    {
        txtSN.text = "Reset Device";
        txtSN.enabled = true;
        yield return new WaitForSeconds(2);
        download = new WWW(WEB_SERVICE_URL + @"mobile/reset_device/" + serial);
        yield return download;
        if (string.IsNullOrEmpty(download.error))
        {
            
            txtSN.text = "Device has been reset";
            yield return new WaitForSeconds(2);
        }
        else
        {
            txtSN.text = "Device fail to reset";
            yield return new WaitForSeconds(2);
        }
        isInit = false;
        isInitPassed = false;
        ConnectErrorCount = 0;
        RegisterState = 0;
        UpdateVersionState = 0;
        CheckVersionState = 0;
        txtSN.enabled = false;
        StartCoroutine(Init());
    }
}
