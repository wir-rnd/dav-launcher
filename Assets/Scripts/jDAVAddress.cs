﻿using System.Collections.Generic;

public class jDAVAddress {
    public string domain { get; set; }
    public int port { get; set; }
    public string directory { get; set; }
}
