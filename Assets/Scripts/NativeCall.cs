﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

public class NativeCall
{
    public static bool InstallResult = false;
    public static bool LaunchResult = false;
    public static IEnumerator Install(string path)
    {
        AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var jo = androidJC.GetStatic<AndroidJavaObject>("currentActivity");
        using (AndroidJavaClass cls = new AndroidJavaClass("wir.rnd.DAV.Main"))
        {
            NativeCall.InstallResult = cls.CallStatic<bool>("Install", new object[] { path });
        }
        yield return new WaitForEndOfFrame();
    }

    public static IEnumerator Launch()
    {
        AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var jo = androidJC.GetStatic<AndroidJavaObject>("currentActivity");
        using (AndroidJavaClass cls = new AndroidJavaClass("wir.rnd.DAV.Main"))
        {
            NativeCall.LaunchResult = cls.CallStatic<bool>("Launch", new object[] { jo });
        }
        yield return new WaitForEndOfFrame();
    }
}
