﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.IO;
using System;


public class Utilities {
    public static string GetMD5Checksum(string path)
    {
        using (var md5 = MD5.Create())
        {
            using (var stream = File.OpenRead(path))
            {
                return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
            }
        }
    }

    public static float GetProgressBarYPos(float h, int tc, int idx)
    {
        float x = h / float.Parse(tc.ToString());
        float y = x / 2f;
        return (x * idx) + y - (h / 2f);
    }

}
